public class App {
    public static void main(String[] args) throws Exception {
        Universidad objUniversidad = new Universidad("UTP","123456");
        //Creación de hasta 5 facultades
        for(int i = 0; i < 5;i++){
            objUniversidad.crear_facultad("Facultad - "+i,i);
        }

        //Consultar las facultades
        for(int i=0; i < 5; i++){
            Facultad objFacultad = objUniversidad.getFacultad(i);
            System.out.println(objFacultad.getNombre()); //consulto las facultades
            objFacultad.crear_carreras("carrera - "+i);
        }
        //String nombre = objUniversidad.getFacultad(0).getNombre();
        //System.out.println(nombre);
    }
}
