public class Universidad {
   
    /*************************
    * Atributos de la clase
    * Atributos de la clase (inician en minúscula)
    **************************/ 
    private String nombre;
    private String nit;
    private Facultad[] facultades;


    /*************************
     * Método constructor
     * Método constructor (es el único método que inicia en mayúscula)
     *************************/
    public Universidad(String nombre, String nit){
        this.nombre = nombre;
        this.nit = nit;
        //Se crea arreglo reservando un espacio en memoria de 5 posiciones para almacenar objetos de tipo facultad
        this.facultades = new Facultad[5];        
    }

    /***************
     * Consultores a las facultades
     * *************/
//Consulta el arreglo de facultades y retorno el objeto
     public Facultad getFacultad(int posicion){
         return this.facultades[posicion];
     }

    /************************
     * Métodos (inician en minúscula)
     ************************/
    public void crear_facultad(String nombre, int posicion){
        //Crear objeto Facultad
        Facultad objFacultad = new Facultad(nombre, this);
        //Ahora mismo el objeto queda local, necesito almacenarlo para que haya la relación de 1 a 5 facultades.
        this.facultades[posicion] = objFacultad; //Almacenará en una posición el objeto Facultad y pasarcelo como parámetro

        //Accede al objeto objFacultad y obtiene el nombre de la facultad
        //String nombre_facultad = objFacultad.getNombre();
        //System.out.println(nombre_facultad);
    

    }
}
