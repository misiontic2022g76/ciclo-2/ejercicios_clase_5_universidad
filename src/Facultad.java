/**
 * Autor: Henry Muñoz
 * Fecha: 07 Julio 2021
 * Empresa: Curso UTP - MINTIC
 * Descripción: Clase que me representa una facultad
 */

public class Facultad {

    /**************
     * Atributos
     ***************/
    private String nombre;
    private Universidad universidad;

    /*****************
     * Consultores
     * getters
     ****************/
    public String getNombre(){
        return this.nombre;
    }

    /******************
     * Modificadores
     *(Setters)
     *****************/

     public void setNombre(String nombre){
         this.nombre = nombre;
     }


    /********************
     * Método constructor
     ********************/
    public Facultad (String nombre, Universidad universidad){
        this.nombre = nombre;
        this.universidad = universidad;
    }

    /********************
     * Métodos (Acciones de la clase)
     ********************/
    public void crear_carreras(String nombre){
        System.out.println("Creando carrera... "+nombre);
    }
    
}
